eject
=====

## About

`eject` is a simple AppleScript, which presents a user with a list of external volumes, and, upon selecting one, ejects it. If there's only one external volume, it ejects it automatically. If no external volumes are present, the script returns exit code 1
